class CreatePromotions < ActiveRecord::Migration[5.1]
  def change
    create_table :promotions do |t|
      t.date :start_date
      t.date :end_date
      t.integer :value
      t.timestamps
    end
  end
end

class Promotion < ApplicationRecord
  validate do |promotion|
    errors[:base] << 'start date must be before end date' if promotion.start_date > promotion.end_date
  end

  def self.apply_joining_promotions(user)
    joining_promotions = Promotion.all.select { |p| p.active? }
    joining_promotions.each { |p| p.apply(user) }
  end

  def active?
    Time.now.between?(start_date, end_date)
  end

  def apply(user)
    balance = user.balance || 0
    balance += value
    user.balance = balance
    user.save
  end
end

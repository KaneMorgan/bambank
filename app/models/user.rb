class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :name, length: { minimum: 1 }
  after_create :apply_promotions

  def apply_promotions
    Promotion.apply_joining_promotions(self)
  end
end

# Bambank

This is a repository for the bamboo loans challenge

## Development Setup

Ensure you have bundler installed

`gem install bundler`

Then run

`bundle install` in the root directory

to install the dependencies

Then run

`rails db:create db:migrate db:seed`

to create the database

finally run `rails s`

The app should be running on [your localhost](http://localhost:3000)

## Testing

The tests are written with rspec.

You can run the tests with `bundle exec rspec`


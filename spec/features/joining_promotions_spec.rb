require 'rails_helper'

RSpec.feature "JoiningPromotions", type: :feature do

  scenario 'Signing up with a €100 joining promotion' do

    FactoryBot.create :promotion

    visit '/'
    expect(page).to have_text('You need to sign in or sign up before continuing.')
    click_link 'Sign up'
    fill_in 'user_name', with: 'test_name'
    fill_in 'user_email', with: 'email@example.com'
    fill_in 'user_password', with: 'test_password'
    fill_in 'user_password_confirmation', with: 'test_password'
    click_button 'Sign up'
    save_and_open_page
    expect(page).to have_text('Welcome! You have signed up successfully')

    within('#balance') do
      expect(page).to have_text '€100'
    end
  end
end

require 'rails_helper'

RSpec.feature "Authenticating", type: :feature do
  scenario 'creating an account' do
    create_user
  end

  scenario 'logging out' do
    user_name = Faker::Name.first_name
    create_user(user_name)
    visit '/'
    expect(page).to have_text(user_name)
    click_link 'Log Out'
    expect(page).not_to have_text(user_name)
  end

  scenario 'logging in' do
    user = FactoryBot.build :user
    create_user(user.name, user.email, user.password)
    click_link('Log Out')
    visit '/'
    fill_in 'user_email', with: user.email
    fill_in 'user_password', with: user.password
    click_button 'Log in'
    expect(page).to have_text('Signed in successfully')
  end

  def create_user(name='test_name', email='test@example.com', password='test_password')
    visit '/'
    expect(page).to have_text('You need to sign in or sign up before continuing.')
    click_link 'Sign up'
    fill_in 'user_name', with: name
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    fill_in 'user_password_confirmation', with: password
    click_button 'Sign up'
    expect(page).to have_text('Welcome! You have signed up successfully')
  end
end

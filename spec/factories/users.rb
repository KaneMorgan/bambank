FactoryBot.define do
  factory :user do
    name { Faker::Name.first_name + ' ' + Faker::Name.last_name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    balance { 0 }
  end
end

FactoryBot.define do
  factory :promotion do
    start_date { Time.now - 1.day }
    end_date { Time.now + 10.days }
    value { 100 }
  end
end

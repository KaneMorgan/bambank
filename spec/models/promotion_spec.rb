require 'rails_helper'

RSpec.describe Promotion, type: :model do

  it 'ensures that the end date is after the start date' do
    promotion = FactoryBot.build :promotion, start_date: Time.now + 1.day, end_date: Time.now
    expect(promotion.save).to be_falsey
  end

  it 'is active if the current date is between the start and end date' do
    promotion = FactoryBot.build :promotion, start_date: Time.now - 1.day, end_date: Time.now + 1.week
    expect(promotion.active?).to be_truthy
  end

  it 'is not active after the end date' do
    promotion = FactoryBot.build :promotion, end_date: Time.now - 1.day
    expect(promotion.active?).to be_falsey
  end

  it 'is not active before the start date' do
    promotion = FactoryBot.build :promotion, end_date: Time.now - 1.day
    expect(promotion.active?).to be_falsey
  end

  it 'applies active promotions to users' do
    u = FactoryBot.create :user, balance: 0
    active_promotion = FactoryBot.create(:promotion, value:100)
    in_active_promotion = FactoryBot.create(:promotion, value:80, end_date: Time.now - 1.day)
    Promotion.apply_joining_promotions(u)

    u = User.find(u.id)
    expect(u.balance).to eq active_promotion.value
  end
end

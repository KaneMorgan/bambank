require 'rails_helper'

RSpec.describe User, type: :model do

  describe "validations" do
    before(:each) do
      @user = FactoryBot.build :user
    end

    it "is valid with the correct properties" do
      expect(@user.valid?).to be_truthy
    end

    it "must have a name" do
      @user.name = nil
      expect(@user.valid?).to be_falsey
    end

    it "cannot have the empty string as a name" do
      @user.name = ''
      expect(@user.valid?).to be_falsey
    end

    it "must have an email address" do
      @user.email = nil
      expect(@user.valid?).to be_falsey
    end
  end

  describe "applying a joining promotion" do
    it "applies joining promotions" do
      promotion_double = class_double('Promotion')
      expect(promotion_double).to receive(:apply_joining_promotions)
      FactoryBot.create(:user)
    end

    it "does not apply promotions on every save after creation" do
      promotion_double = class_double('Promotion')
      user = FactoryBot.create(:user)
      expect(promotion_double).to receive(:apply_joining_promotions)
      user.save
    end
  end
end
